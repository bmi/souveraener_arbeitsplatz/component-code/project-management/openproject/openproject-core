---
sidebar_navigation:
  title: Categories
  priority: 700
description: Document categories in OpenProject.
keywords: file storages, document category, document categories
---

# Document categories

To create or edit document categories in OpenProject, navigate to *Administration → Files → Categories*. Here, you will see all existing values. You can adjust the items within the list by using the options behind the **More (three dots)** menu on the right side. You can also rearrange the order by using the drag-and-drop handle on the left. 

![Documenta categories overview in OpenProject administration](openproject_system_guide_files_categories_overview.png)

## Create new document category

To create a new document category, select the **+ Add** button in the top right corner.

You can then enter a name activate it. Press the **Save** button to save your changes.

![Create new documentation category in OpenProject](openproject_system_guide_files_categories_new.png)

## Edit or remove document category

To **edit** an existing category, either click on the name directly or select the **Edit** option from the **More (three dots)** menu on the right end of the row.

![Edit a documentation category in OpenProject administration](openproject_system_guide_files_categories_edit.png)


To remove a document category, open the **More (three dots)** menu on the right end of the row and click on the **delete** icon.
